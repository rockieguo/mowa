"use strict";

/**
 * @module Middleware_BodyParser
 * @summary Http request body parser middleware
 */

const koaBetterBody = require('koa-better-body');

module.exports = koaBetterBody;